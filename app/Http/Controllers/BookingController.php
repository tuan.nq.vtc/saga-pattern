<?php

namespace App\Http\Controllers;

use App\DTO\CreatingBookingTour;
use App\Http\Requests\BookingHotelRequest;
use App\Models\Booking;
use App\Models\Hotel;
use App\Models\User;
use App\Services\BookingService;
use App\Services\HotelService;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use Throwable;

class BookingController extends Controller
{
	
	public function __construct(
		protected BookingService $service,
		protected UserService $userService,
		protected HotelService $hotelService
	) {
	}
	
	/**
	 * @param BookingHotelRequest $request
	 * @param $id
	 * @return JsonResponse
	 * @throws Throwable
	 */
	public function bookTour(BookingHotelRequest $request, $id)
	{
		/** @var ?User $user */
		$user = $this->userService->find($id);
		/** @var ?Hotel $user */
		$hotel = $this->hotelService->find($request->input('hotel_id'));
		if ($user instanceof User && $hotel instanceof Hotel) {
			return response()->json($this->service->createTour(new CreatingBookingTour(user: $user, hotel: $hotel)));
		}
		
		return response()->json(['message' => 'Record not found.']);
	}
}
