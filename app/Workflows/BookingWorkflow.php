<?php

namespace App\Workflows;

use App\Activities\BookBikeActivity;
use App\Activities\BookFlightActivity;
use App\Activities\CancelBikeActivity;
use App\Activities\CancelFlightActivity;
use App\Models\Booking;
use Throwable;
use Workflow\ActivityStub;
use Workflow\Workflow;
use Generator;

class BookingWorkflow extends Workflow
{
	/**
	 * @param Booking $booking
	 * @return Generator
	 * @throws Throwable
	 */
	public function execute(Booking $booking): Generator
	{
		try {
			$fight = yield ActivityStub::make(BookFlightActivity::class, $booking);
			$this->addCompensation(fn() => ActivityStub::make(CancelFlightActivity::class, $fight));
			$bike = yield ActivityStub::make(BookBikeActivity::class, $booking);
			$this->addCompensation(fn() => ActivityStub::make(CancelBikeActivity::class, $bike));
		} catch (Throwable $e) {
			yield from $this->compensate();
			throw $e;
		}
	}
}
