<?php

namespace App\Services;

use App\Repositories\HotelRepository;

class HotelService
{
	public function __construct(protected HotelRepository $repository)
	{
	}
	
	public function find($id)
	{
		return $this->repository->find($id);
	}
}