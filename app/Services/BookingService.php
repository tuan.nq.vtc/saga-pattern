<?php

namespace App\Services;

use App\Activities\BookFlightActivity;
use App\DTO\CreatingBookingTour;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\BookingDetail;
use App\Models\Flight;
use App\Models\Hotel;
use App\Repositories\BookingRepository;
use App\Workflows\BookingWorkflow;
use Exception;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Throwable;
use Workflow\WorkflowStub;

class BookingService
{
	const BASE_URL = 'https://testapi.io/api/mvc16061987/payments';
	const SUCCESS_URL = '/success';
	const FAILURE_URL = '/false';
	
	public function __construct(protected BookingRepository $repository, protected PendingRequest $request)
	{
		$this->request = Http::withoutVerifying()->baseUrl(self::BASE_URL);
	}
	
	/**
	 * @param CreatingBookingTour $dto
	 * @return Booking
	 * @throws Throwable
	 */
	public function createTour(CreatingBookingTour $dto): Booking
	{
		$user = $dto->getUser();
		$hotel = $dto->getHotel();
		try {
			DB::beginTransaction();
			/** @var Booking $booking */
			$booking = $hotel->booking()->create(['user_id' => $user->id,]);
			$booking->details()->create([
				'type' => BookingDetail::TYPE_PAID,
				'details' => $this->request->post('/' . self::SUCCESS_URL, [])->json()
			]);
			DB::commit();
			return tap($booking, fn($booking) => $this->startWorkflow($booking));
		} catch (Throwable $e) {
			DB::rollBack();
			throw $e;
		}
	}
	
	/**
	 * @param Booking $hotelBooking
	 * @return Booking|void
	 * @throws Throwable
	 */
	public function bookFight(Booking $hotelBooking)
	{
		$this->validateBookingStatus($hotelBooking);
		$hotel = $hotelBooking->resource;
		/** @var ?Flight $fight */
		if ($hotel instanceof Hotel && $fight = $hotel->flights()->first()) {
			try {
				DB::beginTransaction();
				/** @var Booking $fightBooking */
				$fightBooking = $fight->booking()->make();
				$fightBooking->user()->associate($hotelBooking->user)->save();
				$fightBooking->details()->create([
					'type' => BookingDetail::TYPE_PAID,
					'details' => $this->request->post('/' . self::SUCCESS_URL, [])->json()
				]);
				DB::commit();
				return $fightBooking;
			} catch (Throwable $e) {
				DB::rollBack();
				throw $e;
			}
		}
	}
	
	/**
	 * @param Booking $hotelBooking
	 * @return Booking|void
	 * @throws Throwable
	 */
	public function bookBike(Booking $hotelBooking)
	{
		$this->validateBookingStatus($hotelBooking);
		$hotel = $hotelBooking->resource;
		/** @var ?Bike $bike */
		if ($hotel instanceof Hotel && $bike = $hotel->bikes()->first()) {
			try {
				DB::beginTransaction();
				/** @var Booking $bikeBooking */
				$bikeBooking = $bike->booking()->make();
				$bikeBooking->user()->associate($hotelBooking->user)->save();
				
				throw_if($hotelBooking->user->id === 4, 'Poor u ;)');
				
				$bikeBooking->details()->create([
					'type' => BookingDetail::TYPE_PAID,
					'details' => $this->request->post('/' . self::SUCCESS_URL, [])->json()
				]);
				DB::commit();
				return $bikeBooking;
			} catch (Throwable $e) {
				DB::rollBack();
				throw $e;
			}
		}
	}
	
	/**
	 * @param Booking $booking
	 * @return Booking
	 */
	private function startWorkflow(Booking $booking): Booking
	{
		$workflow = WorkflowStub::make(BookingWorkflow::class);
		$workflow->start($booking);
		return $booking;
	}
	
	/**
	 * @param Booking $dependentBooking
	 * @return Booking
	 * @throws Throwable
	 */
	public function cancel(Booking $dependentBooking): Booking
	{
		try {
			DB::beginTransaction();
			$dependentBooking->setAttribute('is_success', false)->save();
			$dependentBooking->details()->create([
				'type' => BookingDetail::TYPE_REFUND,
				'details' => $this->request->post('/' . self::SUCCESS_URL, [])->json()
			]);
			$hotel = $dependentBooking->resource?->hotel;
			if ($hotel instanceof Hotel) {
				$hotel->booking()->each(function (Booking $hotelBooking) {
					$hotelBooking->setAttribute('is_success', false)->save();
					$hotelBooking->details()->create([
						'type' => BookingDetail::TYPE_REFUND,
						'details' => $this->request->post('/' . self::SUCCESS_URL, [])->json()
					]);
				});
			}
			
			DB::commit();
		} catch (Throwable $e) {
			DB::rollback();
			throw $e;
		}
		return $dependentBooking;
	}
	
	/**
	 * @param Booking $hotelBooking
	 * @return void
	 * @throws Throwable
	 */
	private function validateBookingStatus(Booking $hotelBooking): void
	{
		throw_unless($hotelBooking->is_success, new Exception("Booking is failed, cannot continue this action."));
	}
	
}