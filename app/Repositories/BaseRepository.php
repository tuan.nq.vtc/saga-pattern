<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Container\BindingResolutionException;

abstract class BaseRepository
{
	/**
	 * @var Model|Builder $model
	 */
	protected Builder|Model $model;
	
	/**
	 * @throws BindingResolutionException
	 */
	public function __construct()
	{
		$this->setModel();
	}
	
	abstract public function getModel(): string;
	
	/**
	 * @return void
	 * @throws BindingResolutionException
	 */
	public function setModel(): void
	{
		$this->model = app()->make($this->getModel());
	}
	
	public function query(): Builder
	{
		return $this->model->query();
	}
	
	public function find($id): Model|Collection|Builder|array|null
	{
		return $this->query()->find($id);
	}
	
	public function create(array $data): Model|Builder
	{
		return $this->query()->create($data);
	}
}