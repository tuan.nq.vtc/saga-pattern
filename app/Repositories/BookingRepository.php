<?php

namespace App\Repositories;

use App\Models\Booking;

class BookingRepository extends BaseRepository
{
	
	public function getModel(): string
	{
		return Booking::class;
	}
	
}
