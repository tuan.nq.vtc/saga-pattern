<?php

namespace App\Repositories;

use App\Models\BookingDetail;

class BookingDetailRepository extends BaseRepository
{
	
	public function getModel(): string
	{
		return BookingDetail::class;
	}
}
