<?php

namespace App\Repositories;

use App\Models\Hotel;
use App\Models\User;

class HotelRepository extends BaseRepository
{
	
	public function getModel(): string
	{
		return Hotel::class;
	}
}