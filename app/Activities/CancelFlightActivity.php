<?php

namespace App\Activities;

use App\Models\Booking;
use App\Services\BookingService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Workflow\Activity;

class CancelFlightActivity extends Activity
{
	public $tries = 1;
	
	/**
	 * @param Booking $fight
	 * @return Booking
	 * @throws BindingResolutionException
	 */
	public function execute(Booking $fight): Booking
	{
		/** @var BookingService $service */
		$service = app()->make('App\Services\BookingService');
		return $service->cancel($fight);
	}
}