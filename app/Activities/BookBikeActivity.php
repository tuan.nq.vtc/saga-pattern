<?php

namespace App\Activities;

use App\Models\Booking;
use App\Services\BookingService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Throwable;
use Workflow\Activity;
use Workflow\Models\StoredWorkflow;

class BookBikeActivity extends Activity
{
	public $tries = 1;
	
	/**
	 * @throws BindingResolutionException
	 * @throws Throwable
	 */
	public function execute($booking): ?Booking
	{
		/** @var BookingService $service */
		$service = app()->make('App\Services\BookingService');
		return $service->bookBike($booking);
	}
		
}