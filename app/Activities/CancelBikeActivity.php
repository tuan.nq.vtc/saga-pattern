<?php

namespace App\Activities;

use App\Models\Booking;
use App\Services\BookingService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Throwable;
use Workflow\Activity;

class CancelBikeActivity extends Activity
{
	public $tries = 1;
	
	/**
	 * @param Booking $bike
	 * @return Booking
	 * @throws BindingResolutionException
	 * @throws Throwable
	 */
	public function execute(Booking $bike): Booking
	{
		/** @var BookingService $service */
		$service = app()->make('App\Services\BookingService');
		return $service->cancel($bike);
	}
}