<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @property Hotel $hotel
 */
class Flight extends Model
{
	use HasFactory;
	
	protected $fillable = [
		'hotel_id',
		'name',
		'code',
	];
	
	public function hotel(): BelongsTo
	{
		return $this->belongsTo(Hotel::class);
	}
	
	public function booking(): MorphOne
	{
		return $this->morphOne(Booking::class, 'resource');
	}
}