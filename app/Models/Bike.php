<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Bike extends Model
{
	use HasFactory;
	
	protected $fillable = [
		'hotel_id',
		'license_plate',
		'is_used'
	];
	
	protected $casts = [
		'is_used' => 'boolean'
	];
	
	public function hotel(): BelongsTo
	{
		return $this->belongsTo(Hotel::class);
	}
	
	public function booking(): MorphOne
	{
		return $this->morphOne(Booking::class, 'resource');
	}
}