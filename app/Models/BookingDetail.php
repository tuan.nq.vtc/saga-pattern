<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class BookingDetail extends Model
{
	const TYPE_REFUND = 'REFUND';
	const TYPE_PAID = 'PAID';
	protected $fillable = [
		'type',
		'booking_id',
		'details',
	];
	
	protected $casts = [
		'details' => 'array'
	];
	
	public function booking(): BelongsTo
	{
		return $this->belongsTo(Booking::class);
	}
}