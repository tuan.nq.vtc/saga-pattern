<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property boolean $is_success
 * @property User $user
 * @property Flight|Bike|Hotel $resource
 */
class Booking extends Model
{
	
	
	protected $fillable = [
		'user_id',
		'resource_id',
		'resource_type',
		'is_success',
	];
	
	protected $casts = [
		'is_success' => 'boolean',
	];
	
	public function user(): BelongsTo
	{
		return $this->belongsTo(User::class);
	}
	
	public function details(): HasMany
	{
		return $this->hasMany(BookingDetail::class);
	}
	
	public function resource(): MorphTo
	{
		return $this->morphTo('resource');
	}
}