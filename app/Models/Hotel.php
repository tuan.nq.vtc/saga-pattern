<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Hotel extends Model
{
	use HasFactory;
	
	protected $fillable = [
		'name',
		'address',
	];
	
	public function flights(): HasMany
	{
		return $this->hasMany(Flight::class);
	}
	
	public function bikes(): HasMany
	{
		return $this->hasMany(Bike::class);
	}
	
	public function booking(): MorphOne
	{
		return $this->morphOne(Booking::class, 'resource');
	}
}