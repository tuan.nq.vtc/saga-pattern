<?php

namespace App\DTO;

use App\Models\User;

class AddCompensationDTO
{
	public function __construct(protected User $user, protected string $type, protected bool $isSuccess)
	{
	}
	
	public function getUser(): User
	{
		return $this->user;
	}
	
	public function setUser(User $user): void
	{
		$this->user = $user;
	}
	
	public function getType(): string
	{
		return $this->type;
	}
	
	public function setType(string $type): void
	{
		$this->type = $type;
	}
	
	public function isSuccess(): bool
	{
		return $this->isSuccess;
	}
	
	public function setIsSuccess(bool $isSuccess): void
	{
		$this->isSuccess = $isSuccess;
	}
}