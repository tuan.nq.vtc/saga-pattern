<?php

namespace App\DTO;

use App\Models\Hotel;
use App\Models\User;

class CreatingBookingTour
{
	public function __construct(protected User $user, protected Hotel $hotel)
	{
	}
	
	public function getUser(): User
	{
		return $this->user;
	}
	
	public function setUser(User $user): void
	{
		$this->user = $user;
	}
	
	public function getHotel(): Hotel
	{
		return $this->hotel;
	}
	
	public function setHotel(Hotel $hotel): void
	{
		$this->hotel = $hotel;
	}
	
}