<?php

namespace Database\Factories;

use App\Models\Flight;
use App\Models\Hotel;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlightFactory extends Factory
{
	protected $model = Flight::class;
	
	public function definition()
	{
		return [
			'name' => $this->faker->name,
			'code' => $this->faker->unique()->countryCode,
		];
	}
}