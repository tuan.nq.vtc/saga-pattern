<?php

namespace Database\Factories;

use App\Models\BookingDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingDetailFactory extends Factory
{
	protected $model = BookingDetail::class;
	
	public function definition(): array
	{
		return [
			'type' => $this->faker->randomElement(['REFUND', 'PAID']),
			'details' => null, // use your preferred logic to generate details
		];
	}
}