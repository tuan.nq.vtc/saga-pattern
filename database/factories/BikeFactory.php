<?php

namespace Database\Factories;

use App\Models\Bike;
use App\Models\Hotel;
use Illuminate\Database\Eloquent\Factories\Factory;

class BikeFactory extends Factory
{
	protected $model = Bike::class;
	
	public function definition()
	{
		return [
			'license_plate' => '30H-' . $this->faker->regexify('[A-Z0-9]{7}'),
		];
	}
}