<?php

namespace Database\Factories;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
	protected $model = Booking::class;
	
	public function definition(): array
	{
		return [
			'type' => $this->faker->randomElement(['FLIGHT', 'HOTEL', 'RENTAL_BIKE']),
			'is_success' => $this->faker->boolean(),
		];
	}
}