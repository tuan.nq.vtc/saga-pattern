<?php

namespace Database\Seeders;

use App\Models\Bike;
use App\Models\Flight;
use App\Models\Hotel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 */
	public function run(): void
	{
		User::factory(10)->create();
		
		Hotel::factory()->count(10)->create()->each(function ($hotel) {
			$hotel->flights()->saveMany(Flight::factory()->count(rand(1, 5))->make());
			$hotel->bikes()->saveMany(Bike::factory()->count(rand(1, 3))->make());
		});
	}
}
