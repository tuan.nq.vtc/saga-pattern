<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
	/**
	 * Run the migrations.
	 */
	public function up(): void
	{
		Schema::create('hotels', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('address');
			$table->timestamps();
		});
		
		Schema::create('flights', function (Blueprint $table) {
			$table->id();
			$table->foreignId('hotel_id')->index();
			$table->foreign('hotel_id')->references('id')->on('hotels');
			$table->string('name');
			$table->string('code');
			$table->timestamps();
		});
		
		Schema::create('bikes', function (Blueprint $table) {
			$table->id();
			$table->foreignId('hotel_id')->index();
			$table->foreign('hotel_id')->references('id')->on('hotels');
			$table->string('license_plate');
			$table->boolean('is_used')->default(false);
			$table->timestamps();
		});
		
		Schema::create('bookings', function (Blueprint $table) {
			$table->id();
			$table->foreignId('user_id')->index();
			$table->foreign('user_id')->references('id')->on('users');
			$table->uuidMorphs('resource');
			$table->boolean('is_success')->default(true);
			
			$table->timestamps();
		});
		
		Schema::create('booking_details', function (Blueprint $table) {
			$table->id();
			$table->enum('type', ['REFUND', 'PAID']);
			$table->foreignId('booking_id')->index();
			$table->foreign('booking_id')->references('id')->on('bookings');
			
			$table->json('details')->nullable();
			$table->timestamps();
		});
	}
	
	/**
	 * Reverse the migrations.
	 */
	public function down(): void
	{
		Schema::table('bookings', function (Blueprint $table) {
			//
		});
	}
};
