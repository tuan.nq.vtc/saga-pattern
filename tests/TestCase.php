<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
	
	/**
	 * If true, setup has run at least once.
	 * @var boolean
	 */
	protected static bool $setUpHasRunOnce = false;
	/**
	 * After the first run of setUp "migrate:fresh --seed"
	 * @return void
	 */
	public function setUp(): void
	{
		parent::setUp();
		if (!static::$setUpHasRunOnce) {
			Artisan::call('migrate:fresh');
			Artisan::call(
				'db:seed', ['--class' => 'DatabaseSeeder']
			);
			static::$setUpHasRunOnce = true;
		}
	}
}
