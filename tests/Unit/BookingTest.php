<?php

namespace Tests\Unit;


use App\DTO\CreatingBookingTour;
use App\Models\Bike;
use App\Models\Booking;
use App\Models\BookingDetail;
use App\Models\Flight;
use App\Models\Hotel;
use App\Models\User;
use App\Services\BookingService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Throwable;

class BookingTest extends TestCase
{
	use DatabaseTransactions;
	
	public BookingService $service;
	
	public function setUp(): void
	{
		parent::setUp();
		/** @var BookingService $service */
		$this->service = app()->make('App\Services\BookingService');
	}
	
	public function test_it_can_book_a_hotel(): void
	{
		$booking = null;
		$createBookingDTO = $this->initDTO();
		try {
			$booking = $this->service->createTour($createBookingDTO);
		} catch (Throwable $exception) {
			$this->assertDatabaseEmpty(Booking::class);
		}
		
		$this->assertTrue($booking instanceof Booking && $booking->fresh()->is_success);
		$this->assertTrue($booking->user_id == $createBookingDTO->getUser()->id);
		$this->assertTrue(
			$booking->resource_id == $createBookingDTO->getHotel()->id && $booking->resource instanceof Hotel
		);
		$this->assertDatabaseHas(BookingDetail::class, [
			'type' => BookingDetail::TYPE_PAID,
			'booking_id' => $booking->id
		]);
	}
	
	public function test_it_can_book_a_tour(): void
	{
		$hotel = $this->initHotelBooking()->fresh();
		$flightBooking = $this->service->bookFight($hotel);
		$bikeBooking = $this->service->bookBike($hotel);
		
		$this->assertInstanceOf(Booking::class, $flightBooking);
		$this->assertInstanceOf(Flight::class, $flightBooking->resource);
		$this->assertInstanceOf(Booking::class, $bikeBooking);
		$this->assertInstanceOf(Bike::class, $bikeBooking->resource);
		$this->assertDatabaseHas(BookingDetail::class, [
			'type' => BookingDetail::TYPE_PAID,
			'booking_id' => $flightBooking->id
		]);
		$this->assertDatabaseHas(BookingDetail::class, [
			'type' => BookingDetail::TYPE_PAID,
			'booking_id' => $bikeBooking->id
		]);
	}

	public function test_it_can_cancel_a_booking(): void
	{
		$hotel = $this->initHotelBooking()->fresh();
		$flightBooking = $this->service->bookFight($hotel);
		
		$this->assertTrue($hotel->is_success);
		$this->assertTrue($flightBooking->fresh()->is_success);
		
		// begin cancel this booking
		$canceledBooking = $this->service->cancel($flightBooking);
		
		$this->assertTrue($canceledBooking->id == $flightBooking->id);
		$this->assertDatabaseHas(BookingDetail::class, [
			'type' => BookingDetail::TYPE_REFUND,
			'booking_id' => $hotel->id
		]);
		$this->assertDatabaseHas(BookingDetail::class, [
			'type' => BookingDetail::TYPE_REFUND,
			'booking_id' => $flightBooking->id
		]);
		$this->assertFalse($hotel->fresh()->is_success);
		$this->assertFalse($canceledBooking->is_success);
	}
	
	/**
	 * @return Booking|null
	 */
	private function initHotelBooking(): ?Booking
	{
		$booking = null;
		$createBookingDTO = $this->initDTO();
		try {
			$booking = $this->service->createTour($createBookingDTO);
		} catch (Throwable $exception) {
			$this->assertDatabaseEmpty(Booking::class);
		}
		
		return $booking;
	}
	
	private function initDTO(): CreatingBookingTour
	{
		$user = User::factory()->create();
		$hotel = Hotel::factory()->has(Bike::factory())->has(Flight::factory())->create();
		
		return new CreatingBookingTour($user, $hotel);
	}
}
